function [res,theta] = evaluate_fft(coeffs,N,der)
% EVALUATE_FFT evaluates the function in a number of equally spaced points using the fft

O = length(coeffs)-1;
if nargin>2
    coeffs = coeffs.*((1i.*(0:O).').^der);
end
res = zeros(N,1);
res(1:(O+1))=coeffs;
% calculate the result now using the ifft
res = real(ifft(res)*N);
% get the theta axis
theta = linspace(0,2*pi,N+1).';
theta = theta(1:end-1);
% only save the part between 0 and pi
% TODO: it is possible to avoid calculating theta when it is not asked
res = res(theta<=pi);
theta = theta(theta<=pi);
end