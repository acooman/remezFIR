function r = product(c1,c2)
% PRODUCT multiplies two polynomials in cos(kt) and returns the resulting polynomial in cos(kt)

c1 = c1(:);
c2 = c2(:);
% convert to a polynomial in z
C1 = [c1(end:-1:2)/2;c1(1);c1(2:end)/2];
C2 = [c2(end:-1:2)/2;c2(1);c2(2:end)/2];
% perform the convolution of the two polynomials in z
R = conv(C1,C2);
% convert the result back to a polynomial in cos(kt)
r = R((end+1)/2:end)*2;
r(1) = r(1)/2;
end