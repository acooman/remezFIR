function M = Basis(theta,O,der)
theta = theta(:);
if nargin<3
    M = cos( theta.*(0:O) );
else
    M = ones(size(theta)).*((0:O).^der) .* cos( theta.*(0:O) + der*pi/2*ones(length(theta),O+1) );
end
end