function [theta_min,theta_max] = findextrema_fft(coeffs,N)
% FINDEXTREMA_FFT recovers a series of intervals in which the derivative of the function crosses sign
%
%   [theta_min,theta_max,val] = findextrema_fft(x,N)
%
% where coeffs is the list of coefficients and N is the amount of point the fft should use
[res,theta] = evaluate_fft(coeffs,N,1);
ressign = sign(res(2:end-1));
% check for proper zeroes first
if any(ressign==0)
    keyboard
    inds = find(ressign==0);
    ressign(inds)=ressign(inds-1);
end
% get the local minima and local maxima
signchange = ressign(1:end-1)~=ressign(2:end);
% send the results to the outside
theta_min = theta([false;signchange;false;false]);
theta_max = theta([false;false;signchange;false]);
end