function [res,val] = findextrema(coeffs,points_min,points_max)
res = zeros(size(points_min));
for pp=1:length(points_min)
    objfun = @(x) objective(x,coeffs);
    % +- 100*eps because the results with the sum of cosines don't exactly
    % match the results obtained with the fft (error around -200dB)
    finished = false;
    counter = 0;
    while ~finished
        try
            res(pp) = zeroin(objfun,points_min(pp),points_max(pp));
            finished=true;
        catch err
            counter=counter+1;
            if counter<10
                if strcmp(err.identifier,'ZEROIN:nozerocrossing')
                    % when the original interval is not correct due to
                    % differences between the fft and the sum of basis functions,
                    % make the interval a little bigger and try again
                    points_min(pp)=points_min(pp)-50*eps;
                    points_max(pp)=points_max(pp)+50*eps;
                else
                    rethrow(err);
                end
            else
                rethrow(err);
            end
        end
    end
end
val = evaluate(coeffs,res);
end
% objective function
function der=objective(x,coeffs)
der = Basis(x,length(coeffs)-1,1)*coeffs;
end