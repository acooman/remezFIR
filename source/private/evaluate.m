function res = evaluate(x,theta,der)
% EVALUATE evaluates the function in a set of given points
if nargin<3
    der=0;
end
res = Basis(theta,length(x)-1,der)*x;
end