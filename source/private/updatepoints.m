function [points,direct] = updatepoints(points,direct,new,sign)
borders = [-1e-13;points(direct~=sign);pi+10*eps];
% get the borders of the intervals created by the points of opposite sign
[~,intervalind] = histc(new,borders);
low = borders(intervalind);
hig = borders(intervalind+1);
% get the index of the point that has to move
temp = points;
temp(direct~=sign)=Inf;
moveind = find((temp>=low)&(temp<hig));
% set the value of the appropriate point
if ~isempty(moveind)
    if length(moveind)>1
        error('I found multiple points to move!')
    end
    points(moveind)=new;
else
    % it can be that this is empty, in that case, we have to flip a point from
    % the edge of the interval to the other side
    if all(new>points)
        points = [points(2:end);new];
        direct = [direct(2:end);sign];
    elseif all(new<points)
        points = [new;points(1:end-1)];
        direct = [sign;direct(1:end-1)];
    else
        error('something strange')
    end
end
end