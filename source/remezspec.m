classdef remezspec
    
    properties (Access = private)
        int_start
        int_stop
        int_wanted
        int_fixed_up
        int_fixed_down
        int_offset_up
        int_offset_down
    end
    
    properties (Dependent)
        int_offset
        int_fixed
    end
    
    methods
        function obj = remezspec(start,stop,wanted,varargin)
            % CONSTRUCTOR
            % get the length of the first input to compare against later
            L = length(start);
            p=inputParser();
            p.addRequired('int_start' ,@(x) validateattributes(x,{'double'},{'row','size',[1 L]}));
            p.addRequired('int_stop'  ,@(x) validateattributes(x,{'double'},{'row','size',[1 L]}));
            p.addRequired('int_wanted',@(x) validateattributes(x,{'double'},{'row','size',[1 L]}));
            p.addParameter('int_fixed_up'   ,false(1,L),@(x) validateattributes(x,{'logical'},{'row','size',[1 L]}));
            p.addParameter('int_fixed_down' ,false(1,L),@(x) validateattributes(x,{'logical'},{'row','size',[1 L]}));
            p.addParameter('int_offset_up'  ,zeros(1,L),@(x) validateattributes(x,{'double'},{'row','size',[1 L]}));
            p.addParameter('int_offset_down',zeros(1,L),@(x) validateattributes(x,{'double'},{'row','size',[1 L]}));
            p.parse(start,stop,wanted,varargin{:});
            args = p.Results;
            % assign the inputs to the
            fields = fieldnames(args);
            for ff=1:length(fields)
                obj.(fields{ff})=args.(fields{ff})(:);
            end
        end
        %% DISP
        function disp(obj)
            disp('REMEZ specification:')
            for ss=1:length(obj.int_start)
                fprintf('    interval %d: %s to %s   wanted value: %s  \n',ss,num2str(obj.int_start(ss)),num2str(obj.int_stop(ss)),num2str(obj.int_wanted(ss)));
                fprintf('       upper fixed: %d  lower fixed: %d \n',obj.int_fixed_up(ss),obj.int_fixed_down(ss));
                fprintf('       upper offset: %s  lower offset: %s \n',num2str(obj.int_offset_up(ss)),num2str(obj.int_offset_down(ss)));
            end
        end
        function plot(obj,lambda)
            for ss=1:length(obj.int_start)
                % plot the upper level for the interval
                if obj.int_fixed_up(ss)
                    plot([obj.int_start(ss) obj.int_stop(ss)],(obj.int_wanted(ss)+obj.int_offset_up  (ss))*[1 1],'k-','linewidth',2);
                else
                    plot([obj.int_start(ss) obj.int_stop(ss)],(obj.int_wanted(ss)+lambda)*[1 1],'r-','linewidth',2);
                end
                % plot the lower level for the interval
                if obj.int_fixed_down(ss)
                    plot([obj.int_start(ss) obj.int_stop(ss)],(obj.int_wanted(ss)-obj.int_offset_down(ss))*[1 1],'k-','linewidth',2);
                else
                    plot([obj.int_start(ss) obj.int_stop(ss)],(obj.int_wanted(ss)-lambda)*[1 1],'r-','linewidth',2);
                end
            end
        end
        %% FIXED function
        function res = fixed(obj,p,d)
            % FIXED returns a vector of logicals that indicates whether a remez point has a fixed offset or not
            intervals = determineInterval(obj,p);
            if any(intervals==0)
                error('one of the points is not in the specified intervals')
            end
            res = zeros(size(p));
            res(d>0) = obj.int_fixed_up  (intervals(d>0));
            res(d<0) = obj.int_fixed_down(intervals(d<0));
        end
        %% OFFSET function
        function res = offset(obj,p,d)
            % OFFSET returns a vector of wanted offsets for the fixed points
            intervals = determineInterval(obj,p);
            if any(intervals==0)
                error('one of the points is not in the specified intervals')
            end
            res = zeros(size(p));
            res(d>0) = obj.int_offset_up  (intervals(d>0));
            res(d<0) = obj.int_offset_down(intervals(d<0));
        end
        %% WANTED function
        function res = wanted(obj,p)
            % WANTED returns a vector of values with the wanted value for each remez point
            intervals = determineInterval(obj,p);
            if any(intervals==0)
                error('one of the points is not in the specified intervals')
            end
            res = obj.int_wanted(intervals);
        end
        function res = intervalBorders(obj)
            res = [obj.int_start+eps obj.int_stop];
            % remove infinities from the result
            res=res(~isinf(res));
        end
        %% ERROR function
        function res = error(obj,p,val,lambda)
            % ERROR calculates the difference between a given function and the specifications
            
            intervals = determineInterval(obj,p);
            ininterv  = intervals~=0;
            % pre-allocate the result as zeroes
            % only the values in-interval will be used
            res = zeros(size(p));
            % substract the wanted value from the function
            T = intervals(ininterv);
            res(ininterv) = val(ininterv) - obj.int_wanted(T);
            % when the point is fixed, substract the offset from the value
            % when the point is free, substract the lambda-value from the value
            upfixed = false(size(res));
            dnfixed = false(size(res));
            upfixed(ininterv) = obj.int_fixed_up  (T);
            dnfixed(ininterv) = obj.int_fixed_down(T);
            uplimit = zeros(size(res));
            dnlimit = zeros(size(res));
            uplimit(ininterv) = obj.int_offset_up  (T).*upfixed(ininterv) + lambda.*(~upfixed(ininterv));
            dnlimit(ininterv) = obj.int_offset_down(T).*dnfixed(ininterv) + lambda.*(~dnfixed(ininterv));
            upviolation = res>uplimit;
            dnviolation = res<-dnlimit;
            res(upviolation) = res(upviolation) - uplimit(upviolation);
            res(dnviolation) = res(dnviolation) + dnlimit(dnviolation);
            res(~(upviolation|dnviolation))=0;
        end
        %% DETERMINEINTERVAL function
        function res = determineInterval(obj,p)
            % DETERMINEINTERVAL finds out in which specification interval each point is located
            %
            %   res = determineInterval(obj,p)
            % 
            % where p is a vector of points and obj contains the intervals
            % the result is a vector with the same size as p where each
            % value is the index of the interval in which the point is
            % located. (1 for the first interval, 2 for the second, ...)
            % when a point is not in any of the intervals, the resulting
            % value in the res vector is zero
            if ~isvector(p)
                error('the list of points should be a vector')
            end
            N = length(obj.int_start);
            % a point is part of an interval when
            %      it is greater than the start value
            % AND  is is less than or equal to the stop value
            T = ((p(:))>(obj.int_start(:).'))&((p(:))<=(obj.int_stop(:).'));
            % check whether every point lies in a unique interval
            if any(sum(T,2)>1)
                error('one of the points lies in multiple intervals')
            end
            % assign the obtained intevals to the result vector
            inds = find(T.');
            res = zeros(size(p));
            res(floor((inds-1)/N)+1) = mod(inds-1,N)+1;
        end
        %% SETTERS for int_offset and int_fixed
        function obj = set.int_offset(obj,val)
            validateattributes(val,{'double'},{'row','size',[1 length(obj.int_start)]});
            obj.int_offset_up   = val(:);
            obj.int_offset_down = val(:);
        end
        function obj = set.int_fixed(obj,val)
            validateattributes(val,{'logical'},{'row','size',[1 length(obj.int_start)]});
            obj.int_fixed_up   = val(:);
            obj.int_fixed_down = val(:);
        end
    end
    
    methods (Static)
        function checkIntervals(start,stop)
            % CHECKINTERVALS checks whether a given interval vector is valid
            
            % both start and stop should be sorted and increasing
            validateattribute(start,{'double'},{'vector','increasing'})
            validateattribute(stop ,{'double'},{'vector','increasing'})
            % the stop value of each interval should be smaller than or
            % equal to the start value of the next one
            if ~all(stop(1:end-1)>=start(2:end))
                error('overlap detected in the intevals')
            end
        end
    end
end