function [result,lambda] = remez(SP,varargin)
% REMEZ designs a filter starting from specifications
%
%    [result,lambda] =  REMEZ(specification)
%    [result,lambda] =  REMEZ(specification,'ParamName',ParamValue)
%
% The filter is a sum of cosines: 
% H(t) = sum ak * cos(k*t)
% where k goes from 0 to N
% The algorithm determines the coefficients ak to best match
% the specifications described in the specification object
% The specification contains a set of intervals on which a certain value is wanted. 
% For each interval, either a fixed offset from the wanted value is required
% or the difference to the wanted value should be minimised.
% The maximum difference in an interval with a free offset is called lambda
%
% Required Inputs:
%   specification  Default:  CheckFunction: @(x) validateattributes(x,{'remezspec'},{})
%     remezspec object that describes the gabarit of the wanted filter function
% Parameter-Value pairs:
%   order  Default: 60 CheckFunction: @(x) validateattributes(x,{'double'},{'scalar','integer','positive'})
%     order of the filter
%   weightCoefficients  Default: 1 CheckFunction: @(x) validateattributes(x,{'double'},{'vector','nonnan','finite'})
%     coefficients of the weighting function
%   evaluationPoints  Default: 2^16 CheckFunction: @(x) validateattributes(x,{'double'},{'scalar','integer','positive'})
%     number of points on which to evaluate the linear program's constraints
%   errtol  Default: 1e-14 CheckFunction: @(x) validateattributes(x,{'double'},{'scalar','nonnan','finite','positive'})
%     minimum error wanted compared to the gabarit
%   plot  Default: false CheckFunction: @(x) validateattributes(x,{'logical'},{'scalar'})
%     set to true to plot the result
%   iterations  Default: 2000 CheckFunction: @(x) validateattributes(x,{'double'},{'scalar','integer','positive'})
%     maximum amount of iterations
%   dispiter  Default: inf CheckFunction: @(x) validateattributes(x,{'double'},{'scalar','integer','positive'})
%     info will be plotted every xx iterations
%   profile  Default: false CheckFunction: @(x) validateattributes(x,{'logical'},{'scalar'})
%     turn the profiler on or off
% 
% Outputs: 
%   result Type: vector
%     ak coefficients of the filter
%   lambda Type: scalar
%     lambda-value after optimisation
%
% 
% 

p=inputParser();
% remezspec object that describes the gabarit of the wanted filter function
p.addRequired('specification',@(x) validateattributes(x,{'remezspec'},{}))
% order of the filter
p.addParameter('order'  ,60    ,@(x) validateattributes(x,{'double'},{'scalar','integer','positive'}))
% coefficients of the weighting function
p.addParameter('weightCoefficients'  ,1    ,@(x) validateattributes(x,{'double'},{'vector','nonnan','finite'}))
% number of points on which to evaluate the linear program's constraints
p.addParameter('evaluationPoints',2^16  ,@(x) validateattributes(x,{'double'},{'scalar','integer','positive'}))
% minimum error wanted compared to the gabarit
p.addParameter('errtol',1e-14  ,@(x) validateattributes(x,{'double'},{'scalar','nonnan','finite','positive'}))
% set to true to plot the result
p.addParameter('plot',false,@(x) validateattributes(x,{'logical'},{'scalar'}));
% maximum amount of iterations
p.addParameter('iterations',2000,@(x) validateattributes(x,{'double'},{'scalar','integer','positive'}));
% info will be plotted every xx iterations
p.addParameter('dispiter',inf,@(x) validateattributes(x,{'double'},{'scalar','integer','positive'}));
% turn the profiler on or off
p.addParameter('profile',false,@(x) validateattributes(x,{'logical'},{'scalar'}));
p.parse(SP,varargin{:});
args = p.Results;

%% preallocate some result matrices and vectors
iter = args.iterations;

points = zeros(args.order+2,iter+1);
direct = zeros(args.order+2,iter+1);
maxviol= zeros(1,iter);
lambda = zeros(1,iter);

%% calculate the coefficients for the weight function
wcoeffs = args.weightCoefficients;

%% initialise the remez points
points(:,1) = linspace(0,pi,args.order+2);
direct(:,1) = cos((1:(args.order+2))*pi);

if args.profile
    profile on
end
for ii=1:iter
    % calculate the coefficients of the polynomial
    % P(x1) = lambda/W(x1) -> |  1  cos(x1) ... cos(N*x1)  -1/W(x1) |   | a0 |   |      0      |
    % P(x2) = 0            -> |  1  cos(x2) ... cos(N*x2)   0       |   | a1 |   |      0      |
    % P(x3) = lambda/W(x3) -> |  1  cos(x3) ... cos(N*x3)  -1/W(x3) |   | a2 |   |      0      |
    % P(x4) = 0            -> |  1  cos(x4) ... cos(N*x4)   0       |   | a3 |   |      0      |
    %                         | ...  ...    ...   ...      ...      | * | .. | = |     ...     |
    % P(xm) = (1+E)/W(xm)  -> |  1  cos(xm) ... cos(N*xm)   0       |   | .. |   | (1+E)/W(xm) |
    % P(xn) = (1-E)/W(xn)  -> |  1  cos(xn) ... cos(N*xn)   0       |   | aN |   | (1-E)/W(xn) |
    % P(xo) = (1+E)/W(xo)  -> |  1  cos(xo) ... cos(N*xo)   0       |   | e  |   | (1-E)/W(xo) |
    %
    %                                       K             EPS            x              O
    fixed  = SP.fixed (points(:,ii),direct(:,ii));
    offset = SP.offset(points(:,ii),direct(:,ii));
    wanted = SP.wanted(points(:,ii));
    K  = Basis(points(:,ii),args.order);
    W  = evaluate(wcoeffs,points(:,ii));
    % get the matrix of basis functions evaluated in the points
    x = [K -(direct(:,ii)).*(~fixed)./W]\((wanted+direct(:,ii).*offset.*fixed)./W);
    % save the lambda-value for later plotting
    lambda(ii) = x(end);
    coeffs     = x(1:end-1);
    
    %% find the biggest violation of the constraints
    % find the minima and maxima of the function
    [t_min,t_max]  = findextrema_fft(product(coeffs,wcoeffs) ,args.evaluationPoints);
    [thetaerr,val] = findextrema    (product(coeffs,wcoeffs) ,t_min,t_max );
    % add the edges of the intervals to the list of points
    extrapoints = [ 0 ; pi ; SP.intervalBorders ];
    thetaerr = [ thetaerr ; extrapoints ];
    val = [val;evaluate(product(coeffs,wcoeffs),extrapoints)];
    % calculate the error with the specifications
    err = SP.error(thetaerr,val,lambda(ii));
    % find the maximum error
    [maxviol(ii),loc]=max(abs(err));
    violtheta=thetaerr(loc);
    violsig = sign(err(loc));
    % check whether the error is small enough. if it is, we can quit
    if maxviol(ii)<args.errtol
        break
    end
    % display info if needed
    if mod(ii,args.dispiter)==0
        fprintf('iteration %d lambda is: %e error is: %e\n',ii,lambda(ii),maxviol(ii));
    end
    % move the closest viable interpolation point to the maximum
    [points(:,ii+1),direct(:,ii+1)] = updatepoints( points(:,ii) , direct(:,ii) , violtheta , violsig );
end
if args.profile
    profile off
    profile viewer
end

% save only the usefull iterations, when the loop was broken early
points  =  points(:,1:ii);
direct  =  direct(:,1:ii);
maxviol = maxviol(1,1:ii);
lambda  =  lambda(1,1:ii);
iter = ii;

%% plot the evolution of the error and of lambda

if args.plot
    figure(3132)
    subplot(121)
    hold on
    plot(1:iter,log10(maxviol),'+');
    xlabel('iteration')
    ylabel('maximum error')
    subplot(122)
    plot(1:iter,lambda);
    xlabel('iteration')
    ylabel('lambda')
end

%% plot the obtained function
if args.plot
    figure(31654621)
    hold on
    % plot the function
    [res,thetatest] = evaluate_fft(product(coeffs,wcoeffs),args.evaluationPoints);
    plot(thetatest(res>0),(res(res>0)),'b');
    plot(thetatest(res<0),(res(res<0)),'r');
    %  plot the interpolation points
    plot(points(direct(:,end-1)>0,end-1),(evaluate(product(coeffs,wcoeffs),points(direct(:,end-1)>0,end-1))),'rx')
    plot(points(direct(:,end-1)<0,end-1),(evaluate(product(coeffs,wcoeffs),points(direct(:,end-1)<0,end-1))),'b+')
    % plot the local minima and maxima
    plot(thetaerr ,(evaluate(product(coeffs,wcoeffs),thetaerr )),'m*');
    plot(violtheta,(evaluate(product(coeffs,wcoeffs),violtheta)),'ms');
end

% the result of the whole algorithm is the product of the coefficients and the weight
result = product(coeffs,wcoeffs);
lambda = lambda(end);

end

% @generateHelp
% @Tagline designs a filter starting from specifications

% @Description The filter is a sum of cosines: 
% @Description     H(t) = sum ak * cos(k*t)
% @Description   where k goes from 0 to N
% @Description The algorithm determines the coefficients ak to best match
% @Description the specifications described in the specification object
% @Description 
% @Description The specification contains a set of intervals on which a certain value is wanted. 
% @Description For each interval, either a fixed offset from the wanted value is required
% @Description or the difference to the wanted value should be minimised.
% @Description The maximum difference in an interval with a free offset is called lambda

% @Outputs{1}.Description ak coefficients of the filter
% @Outputs{1}.Type vector
% @Outputs{2}.Description lambda-value after optimisation
% @Outputs{2}.Type scalar


