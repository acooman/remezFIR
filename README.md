# remezFIR

Matlab code to design FIR filters using the Remez exchange algorithm. 

This small toolbox contains two main parts: 

__remezspec__: 	a class to describe the FIR filter specifications.

__remez__: 	a function to design the filter that satisfies those specifications.

Compared to the built-in remez implementation in Matlab, this implementation allows:

- Specifying frequency intervals where a fixed offset from the specification is wanted
- Designing FIR filters where the frequency response is always larger than 0
- Adding a polynomial weight function, which can be used to fix transmission zeros in the filter

## remezspec

Remezspec is a class that describes the specifications of the wanted FIR filter. 
It contains a set of intervals on which the filter is specified. Outside of the intervals, the filter can take any value.

Each interval has a wanted value. The filter function will try to get close to the wanted value. 
There are two different ways to constrain the difference between the wanted value and the value of the filter function:

### The difference is fixed and specified: 

In this case, two fixed offsets are specified by the user. On such an interval, the filter will satisfy the following constraint:

```math
wanted - {offset}_{bot} <= H(\theta) <= wanted + {offset}_{top}
```

where $`wanted`$ is the wanted value for the interval, $`offset_{top}`$ is the maximum offset the filter can have above the wanted value. $`offset_{bot}`$ is the maximum offset the filter will have below the wanted value.

### The difference needs to be minimised

In this case, the filter will try to go as close as possible to the wanted value. The maximum difference between the filter and the wanted value is called $`\lambda`$. The remez function will minimise $`\lambda`$ and the filter in the interval will satisfy the following constraint:
```math
wanted - \lambda <= H(\theta) <= wanted + \lambda
```

### Mixed intervals

The user can also choose to fix only the top or the bottom of an interval. One can, for example only fix the offset above the wanted value and minimise the offset below the wanted value:
```math
wanted - \lambda <= H(\theta) <= wanted + offset_{top}
```
This assymmertric offset is very usefull when the filter function needs to be positive, but as small as possible. 
In that case we obtain the following constraint
```math
0 <= H(\theta) <= \lambda
```
with $`wanted=0`$ and $`offset_{bot}=0`$

### usage

In its minimal configuration, the constructor for remezspec takes three equal-length row vectors.
The first argument is a vector which contains the start value for each interval.
the second argument is a vector which contains the stop value for each interval.
the thirs argument is a vector which contains the wanted value for each interval.
```matlab
SP = remezspec([0 pi/2],[pi/4 pi],[1 0])
```

In this default configuration, the specification has no fixed offsets, so the remez function will act like the built-in remez function from matlab.

You can set the offset and fix some of the points by setting the `int_fixed` and `int_offset` of the object.

### methods 

In the following, `p` is a vector of $`\theta`$-values inside the intervals and `d` is a vector of +-1 which indicates whether the point is above or below the specification in the filter function.

The remezspec class has the following methods:

__SP.fixed(p,d)__ returns a vector of logicals which indicate whether the offset of a point to the specification is fixed or whether it needs to be optimised

__SP.offset(p,d)__ returns a vector of offset values for each point in p

__SP.wanted(p)__ returns a vector which contains the wanted value for each point in the list p

__SP.error(p,H(p),lambda__ calculated the error of the function with respect to the specification

__SP.plot(lambda)__ plots the specification


## remez function

the remez function performs the actual optimisation. The FIR filter response is described by the following function
```math
H(\theta) = \sum_{k=0}^N a_k * cos(k * \theta)
```
The remez function finds the $`a_k`$ values such that $`H(\theta)`$ satisfies the constraints described in the remezspec object.

### Usage

```matlab
coeffs = remez(SP,'order',20);
```

### The algorithm

A set of N+2 initial points $`\theta_1 ... \theta_{M}`$ is chosen.
Also, an alternating sequence $`d_1 ... d_M`$ is chosen, where, when $`d_n=+1`$ then $`d_{n+1}=-1`$

#### Finding the coefficients for the given set of points

For that set of points and directions, the following equation is solved:
```math
\left[\begin{array}{cccccc}
1 & \cos\left(\theta_{1}\right) & \cos\left(2\theta_{1}\right) & \cdots & \cos\left(N\theta_{1}\right) & -d_{1}\overline{f\left(\theta_{1},d_1\right)}\\
1 & \cos\left(\theta_{2}\right) & \cos\left(2\theta_{2}\right) & \cdots & \cos\left(N\theta_{2}\right) & -d_{2}\overline{f\left(\theta_{2},d_2\right)}\\
\vdots & \vdots & \vdots & \vdots & \vdots\\
1 & \cos\left(\theta_{M}\right) & \cos\left(2\theta_{M}\right) & \cdots & \cos\left(N\theta_{M}\right) & -d_{M}\overline{f\left(\theta_{M},d_M\right)}
\end{array}\right]\left[\begin{array}{c}
a_{0}\\
a_{1}\\
\vdots\\
a_{N}\\
\lambda
\end{array}\right]=\left[\begin{array}{c}
w\left(\theta_{1}\right)+d_{1}f\left(\theta_{1},d_1\right)o\left(\theta_{1},d_1\right)\\
w\left(\theta_{2}\right)+d_{2}f\left(\theta_{2},d_2\right)o\left(\theta_{2},d_2\right)\\
\vdots\\
w\left(\theta_{M}\right)+d_{M}f\left(\theta_{M},d_M\right)o\left(\theta_{M},d_M\right)
\end{array}\right]
```
where $`w(\theta)`$ returns the wanted value according to the specifications. 

$`o(\theta,d)`$ is the offset from the wanted value for point $`\theta`$ and direction $`d`$ according to the specifications.

$`f(\theta,d)`​$ is the function which returns whether a point is fixed or not. When the point has a fixed offset, $`f(\theta,d)=1`​$ when the distance to the wanted value needs to be minimised $`f(\theta,d)=0`​$. $`\overline{f(\theta,d)}`​$ is the opposite of $`f(\theta,d)`​$.

#### Determining the maximum error

Because the wanted values on the intervals are constant for each interval, we know that the maximum difference between the filter function and the specification will either be at the edges of each interval or in a local maximum or minimum of the filter function.

The local minima and maxima of $`H(\theta)`$ are calculated by finding the zero-crossings of the first derivative in a two-step approach. 

First, the frequency response of the filter is determined on a frequency grid which is linearly spaced in $`\theta`$. This can be done fast using the FFT. From this frequency response, the intervals in which a zero-crossing of $`H(\theta)`$ takes place are determined. The amount of points used in the FFT is controlled by the `evaluationPoints` parameter.

In the second step, the intervals obtained with the FFT are refined using the zeroin function.

In those local minima and maxima, the error with respect to the specification is calculated.

#### Updating the points

The maximum of the error is determined. The point in the list with its $`\theta`$-value closest to the maximum of the error and with the same sign is moved

This process is repeated untill the error is below the error tolerance level. The error tolerance level is controlled by the `errortol` parameter.

#### Using a weight

__TODO:__ explain weighting polynomial

The coefficients of the weighting polynomial are passed into the `weightCoefficients` parameter of the algorithm.


## Example

```matlab
SP = remezspec([-inf pi/2],[pi/2-0.1 inf],[1 0])
```

now call the remez function to design the filter

```matlab
coeffs = remez(SP,'order',12);
```

```matlab
theta = linspace(0,pi,1001).';
res = cos((0:(length(coeffs)-1)).*theta)*coeffs;
```

plot the result

```matlab
plot(theta,res);
```

__TODO:__ add the result of this plot in a figure