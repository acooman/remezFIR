% in this example, we show a more advanced use of the remez algorithm
% we use a fixed offset in the passband of the filter and a free offset in
% the stopband. 
% this way, the passband ripple can be specified exactly, while maximising
% the stopband suppression
clear variables
close all
clc

% first call the remezspec function to generate a filter specification
% we have two intervals:
% interval 1 goes from -inf to pi/2-0.1
%  the value we want there is 1
% interval 2 goes from pi/2 to inf
%  the value we want there is 0
SP = remezspec([-inf pi/2],[pi/2-0.1 inf],[1 0]);
% tell the specification that the first interval has a fixed offset
SP.int_fixed = [true false];
% and specify the wanted offset of 0.1. 
% Because the second band is free, the offset doesn't matter there
SP.int_offset= [0.1 0];


% now call the remez function to design the filter
[coeffs,lambda] = remez(SP,'order',12);

% evaluate the filter.
% the returned coefficients are the ak coefficients in the sum
%   H(t) = sum ak * cos(k*t)
theta = linspace(0,pi,1001).';
res = cos((0:(length(coeffs)-1)).*theta)*coeffs;

% plot the result
figure(1)
hold on
plot(theta,res);
plot(SP,lambda)

