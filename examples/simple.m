% in this script, we show how to use the remez algorithm to design a simple lowpass filter.
% no special features are used, so the difference to the specification is
% minimised for all the intervals
clear variables
close all
clc

% first call the remezspec function to generate a filter specification
% we have two intervals:
% interval 1 goes from -inf to pi/2-0.1
%  the value we want there is 1
% interval 2 goes from pi/2 to inf
%  the value we want there is 0
SP = remezspec([-inf pi/2],[pi/2-0.1 inf],[1 0])

% now call the remez function to design the filter
coeffs = remez(SP,'order',12);

% evaluate the filter.
% the returned coefficients are the ak coefficients in the sum
%   H(t) = sum ak * cos(k*t)
theta = linspace(0,pi,1001).';
res = cos((0:(length(coeffs)-1)).*theta)*coeffs;

% plot the result
figure(1)
plot(theta,res);